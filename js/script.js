/**
 * Класс, объекты которого описывают параметры гамбургера. 
 * 
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */


function Hamburger(size, stuffing) {
    try {

        if (!size || !stuffing) {
            throw new HamburgerException("no size or stuffing given");
        } else if (!(size instanceof Object) || !(stuffing instanceof Object)) {
            throw new HamburgerException("ivalid type of size or stuffing given");
        } else if (size != Hamburger.SIZE_SMALL && size != Hamburger.SIZE_LARGE) {
            throw new HamburgerException("select correct size: Small or Large");
        } else if (stuffing != Hamburger.STUFFING_CHEESE && size != Hamburger.STUFFING_SALAD && size != Hamburger.STUFFING_POTATO) {
            throw new HamburgerException("select correct stuffing: Cheese, Salad or Potato");
        } else {
            this.size = size;
            this.stuffing = stuffing;
        }
    } catch (e) {
        console.log(e.message);
    }

}
/* Размеры, виды начинок и добавок */

Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40
};

Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20
};

Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    calories: 10
};

Hamburger.TOPPING_MAYO = {
    price: 20,
    calories: 5
};

Hamburger.TOPPING_SPICE = {
    price: 15,
    calories: 0
};
//Hamburger.TOPPING_SPICE_2 = {
//    price: 15,
//    calories: 12
//};

Object.freeze(Hamburger.SIZE_SMAL);
Object.freeze(Hamburger.SIZE_LARGE);
Object.freeze(Hamburger.STUFFING_CHEESE);
Object.freeze(Hamburger.STUFFING_SALAD);
Object.freeze(Hamburger.STUFFING_POTATO);
Object.freeze(Hamburger.TOPPING_MAYO);
Object.freeze(Hamburger.TOPPING_SPICE);

Hamburger.TOPPING_SPICE.name = "Пишу что хочу!";
console.log(Hamburger.TOPPING_SPICE.name);
/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    var price = 0;
    for (var key in this) {
        if (this[key].price) {
            price += this[key]["price"];
        }
    }
    if (this.toppings) {
        for (var i = 0; i < this.toppings.length; i++) {
            price += this.toppings[i].price;
        }
    }
    return price;
};

/*
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 * 
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!topping) {
            throw new HamburgerException("no topping given");
        } else if (topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE) {
            throw new HamburgerException("select correct topping: Mayo or Spice");
        } else if (this.toppings && this.toppings.indexOf(topping) > -1) {
            throw new HamburgerException("topping already given");
        } else if (this.toppings && this.toppings.indexOf(topping) === -1) {
            this.toppings.push(topping);
        } else {
            this.toppings = [];
            this.toppings.push(topping);
        }

    } catch (e) {
        console.log(e.message);
    }
};

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (!topping) {
            throw new HamburgerException("no topping specified");
        } else if (this.toppings && this.toppings.indexOf(topping) === -1) {
            throw new HamburgerException("this topping wasn't added");
        } else {
            this.toppings.splice(this.toppings.indexOf(topping), 1);
        }
    } catch (e) {
        console.log(e.message);
    }
};


/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.toppings;
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};


/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    var caloriesValue = 0;
    for (var key in this) {
        if (this[key].calories) {
            caloriesValue += this[key]["calories"];
        }
    }
    if (this.toppings) {
        for (var i = 0; i < this.toppings.length; i++) {
            caloriesValue += this.toppings[i].calories;
        }
    }
    return caloriesValue;
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException(message) {
    this.message = message;
}


// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
//var hamburger = new Hamburger(Hamburger.STUFFING_CHEESE, Hamburger.SIZE_LARGE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
//hamburger.addTopping(Hamburger.TOPPING_MAYO); // ->проверка 

// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log(hamburger.toppings);
// А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
//hamburger.removeTopping(Hamburger.TOPPING_SPICE); // ->проверка 
console.log("Have %d toppings", hamburger.getToppings().length); // 1
